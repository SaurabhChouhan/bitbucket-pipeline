package com.pipeline.testing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PipelineTesting {

	public static void main(String[] args) {
		SpringApplication.run(PipelineTesting.class, args);
	}
}
