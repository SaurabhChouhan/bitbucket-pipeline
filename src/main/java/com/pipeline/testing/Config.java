package com.pipeline.testing;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.pipeline.testing")
@Configuration
public class Config {
  public static final String name = "test for pipelines.";

}
